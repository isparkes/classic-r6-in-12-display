EESchema Schematic File Version 4
LIBS:Nixie 12-9-cache
EELAYER 26 0
EELAYER END
$Descr User 14208 8268
encoding utf-8
Sheet 1 1
Title "Classic Rev6 IN-12 Display Board"
Date "2019-06-07"
Rev "Rev1"
Comp ""
Comment1 "Thanks to Mike Sauve"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4500 3800 1100 3800
Wire Wire Line
	1100 3800 1100 1600
Wire Wire Line
	1100 1600 1900 1600
Wire Wire Line
	4500 3800 4500 5000
Text Label 3175 3800 0    70   ~ 0
A1
Wire Wire Line
	3900 1600 3100 1600
Wire Wire Line
	3100 1600 3100 3700
Wire Wire Line
	3100 3700 4600 3700
Wire Wire Line
	4600 3700 4600 5000
Text Label 3225 3700 0    70   ~ 0
A2
Wire Wire Line
	6500 1600 5700 1600
Wire Wire Line
	5700 1600 5700 4000
Wire Wire Line
	5700 4000 4700 4000
Wire Wire Line
	4700 4000 4700 5000
Text Label 5075 4000 0    70   ~ 0
A3
Wire Wire Line
	7300 1600 7300 4100
Wire Wire Line
	7300 4100 4800 4100
Wire Wire Line
	8100 1600 7300 1600
Wire Wire Line
	4800 4100 4800 5000
Text Label 5075 4100 0    70   ~ 0
A4
Wire Wire Line
	11100 1600 10300 1600
Wire Wire Line
	10300 1600 10300 4200
Wire Wire Line
	10300 4200 4900 4200
Wire Wire Line
	4900 4200 4900 5000
Text Label 5075 4200 0    70   ~ 0
A5
Wire Wire Line
	5000 4300 11900 4300
Wire Wire Line
	12700 1600 11900 1600
Wire Wire Line
	11900 1600 11900 4300
Wire Wire Line
	5000 4300 5000 5000
Text Label 5125 4300 0    70   ~ 0
A6
Wire Wire Line
	1400 2500 1400 2600
Wire Wire Line
	1400 2600 3400 2600
Wire Wire Line
	3400 2600 3400 2500
Wire Wire Line
	3400 2600 6000 2600
Wire Wire Line
	6000 2600 6000 2500
Wire Wire Line
	6000 2600 7600 2600
Wire Wire Line
	7600 2600 7600 2500
Wire Wire Line
	7600 2600 10600 2600
Wire Wire Line
	10600 2600 10600 2500
Wire Wire Line
	10600 2600 12200 2600
Wire Wire Line
	12200 2600 12200 2500
Wire Wire Line
	3400 5000 3400 2600
Connection ~ 3400 2600
Connection ~ 6000 2600
Connection ~ 7600 2600
Connection ~ 10600 2600
Text Label 2700 2600 0    70   ~ 0
K1
Wire Wire Line
	1500 2500 1500 2700
Wire Wire Line
	1500 2700 3500 2700
Wire Wire Line
	3500 2700 3500 2500
Wire Wire Line
	3500 2700 6100 2700
Wire Wire Line
	6100 2700 6100 2500
Wire Wire Line
	6100 2700 7700 2700
Wire Wire Line
	7700 2700 7700 2500
Wire Wire Line
	7700 2700 10700 2700
Wire Wire Line
	10700 2700 10700 2500
Wire Wire Line
	10700 2700 12300 2700
Wire Wire Line
	12300 2700 12300 2500
Wire Wire Line
	3500 5000 3500 2700
Connection ~ 3500 2700
Connection ~ 6100 2700
Connection ~ 7700 2700
Connection ~ 10700 2700
Text Label 2700 2700 0    70   ~ 0
K2
Wire Wire Line
	1600 2500 1600 2800
Wire Wire Line
	1600 2800 3600 2800
Wire Wire Line
	3600 2800 3600 2500
Wire Wire Line
	3600 2800 6200 2800
Wire Wire Line
	6200 2800 6200 2500
Wire Wire Line
	6200 2800 7800 2800
Wire Wire Line
	7800 2800 7800 2500
Wire Wire Line
	7800 2800 10800 2800
Wire Wire Line
	10800 2800 10800 2500
Wire Wire Line
	10800 2800 12400 2800
Wire Wire Line
	12400 2800 12400 2500
Wire Wire Line
	3600 2800 3600 5000
Connection ~ 3600 2800
Connection ~ 6200 2800
Connection ~ 7800 2800
Connection ~ 10800 2800
Text Label 2725 2800 0    70   ~ 0
K3
Wire Wire Line
	1700 2500 1700 2900
Wire Wire Line
	1700 2900 3700 2900
Wire Wire Line
	3700 2900 3700 2500
Wire Wire Line
	3700 2900 6300 2900
Wire Wire Line
	6300 2900 6300 2500
Wire Wire Line
	6300 2900 7900 2900
Wire Wire Line
	7900 2900 7900 2500
Wire Wire Line
	7900 2900 10900 2900
Wire Wire Line
	10900 2900 10900 2500
Wire Wire Line
	10900 2900 12500 2900
Wire Wire Line
	12500 2900 12500 2500
Wire Wire Line
	3700 5000 3700 2900
Connection ~ 3700 2900
Connection ~ 6300 2900
Connection ~ 7900 2900
Connection ~ 10900 2900
Text Label 2725 2900 0    70   ~ 0
K4
Wire Wire Line
	1800 2500 1800 3000
Wire Wire Line
	1800 3000 3800 3000
Wire Wire Line
	3800 3000 3800 2500
Wire Wire Line
	3800 3000 6400 3000
Wire Wire Line
	6400 3000 6400 2500
Wire Wire Line
	6400 3000 8000 3000
Wire Wire Line
	8000 3000 8000 2500
Wire Wire Line
	8000 3000 11000 3000
Wire Wire Line
	11000 3000 11000 2500
Wire Wire Line
	11000 3000 12600 3000
Wire Wire Line
	12600 3000 12600 2500
Wire Wire Line
	3800 3000 3800 5000
Connection ~ 3800 3000
Connection ~ 6400 3000
Connection ~ 8000 3000
Connection ~ 11000 3000
Text Label 2725 3000 0    70   ~ 0
K5
Wire Wire Line
	1900 2500 1900 3100
Wire Wire Line
	1900 3100 3900 3100
Wire Wire Line
	3900 3100 3900 2500
Wire Wire Line
	3900 3100 6500 3100
Wire Wire Line
	6500 3100 6500 2500
Wire Wire Line
	6500 3100 8100 3100
Wire Wire Line
	8100 3100 8100 2500
Wire Wire Line
	8100 3100 11100 3100
Wire Wire Line
	11100 3100 11100 2500
Wire Wire Line
	11100 3100 12700 3100
Wire Wire Line
	12700 3100 12700 2500
Wire Wire Line
	3900 5000 3900 3100
Connection ~ 3900 3100
Connection ~ 6500 3100
Connection ~ 8100 3100
Connection ~ 11100 3100
Text Label 2700 3100 0    70   ~ 0
K6
Wire Wire Line
	2000 2500 2000 3200
Wire Wire Line
	2000 3200 4000 3200
Wire Wire Line
	4000 3200 4000 2500
Wire Wire Line
	4000 3200 6600 3200
Wire Wire Line
	6600 3200 6600 2500
Wire Wire Line
	6600 3200 8200 3200
Wire Wire Line
	8200 3200 8200 2500
Wire Wire Line
	8200 3200 11200 3200
Wire Wire Line
	11200 3200 11200 2500
Wire Wire Line
	11200 3200 12800 3200
Wire Wire Line
	12800 3200 12800 2500
Wire Wire Line
	4000 3200 4000 5000
Connection ~ 4000 3200
Connection ~ 6600 3200
Connection ~ 8200 3200
Connection ~ 11200 3200
Text Label 2700 3200 0    70   ~ 0
K7
Wire Wire Line
	2100 2500 2100 3300
Wire Wire Line
	2100 3300 4100 3300
Wire Wire Line
	4100 3300 4100 2500
Wire Wire Line
	4100 3300 6700 3300
Wire Wire Line
	6700 3300 6700 2500
Wire Wire Line
	6700 3300 8300 3300
Wire Wire Line
	8300 3300 8300 2500
Wire Wire Line
	8300 3300 11300 3300
Wire Wire Line
	11300 3300 11300 2500
Wire Wire Line
	11300 3300 12900 3300
Wire Wire Line
	12900 3300 12900 2500
Wire Wire Line
	4100 5000 4100 3300
Connection ~ 4100 3300
Connection ~ 6700 3300
Connection ~ 8300 3300
Connection ~ 11300 3300
Text Label 2700 3300 0    70   ~ 0
K8
Wire Wire Line
	2200 2500 2200 3400
Wire Wire Line
	2200 3400 4200 3400
Wire Wire Line
	4200 3400 4200 2500
Wire Wire Line
	4200 3400 6800 3400
Wire Wire Line
	6800 3400 6800 2500
Wire Wire Line
	6800 3400 8400 3400
Wire Wire Line
	8400 3400 8400 2500
Wire Wire Line
	8400 3400 11400 3400
Wire Wire Line
	11400 3400 11400 2500
Wire Wire Line
	11400 3400 13000 3400
Wire Wire Line
	13000 3400 13000 2500
Wire Wire Line
	4200 3400 4200 5000
Connection ~ 4200 3400
Connection ~ 6800 3400
Connection ~ 8400 3400
Connection ~ 11400 3400
Text Label 2725 3400 0    70   ~ 0
K9
Wire Wire Line
	2300 2500 2300 3500
Wire Wire Line
	2300 3500 4300 3500
Wire Wire Line
	4300 3500 4300 2500
Wire Wire Line
	4300 3500 6900 3500
Wire Wire Line
	6900 3500 6900 2500
Wire Wire Line
	6900 3500 8500 3500
Wire Wire Line
	8500 3500 8500 2500
Wire Wire Line
	8500 3500 11500 3500
Wire Wire Line
	11500 3500 11500 2500
Wire Wire Line
	11500 3500 13100 3500
Wire Wire Line
	13100 3500 13100 2500
Wire Wire Line
	4300 5000 4300 3500
Connection ~ 4300 3500
Connection ~ 6900 3500
Connection ~ 8500 3500
Connection ~ 11500 3500
Text Label 2725 3500 0    70   ~ 0
K0
Wire Wire Line
	8900 1100 4700 1100
Wire Wire Line
	1000 4600 2600 4600
Wire Wire Line
	2600 4600 2600 5000
Connection ~ 4700 1100
Wire Wire Line
	9600 2400 9600 3800
Wire Wire Line
	9600 3800 9200 3800
Wire Wire Line
	10000 1400 10000 4000
Wire Wire Line
	10000 4000 9700 4000
Wire Wire Line
	5300 2400 5300 3900
Wire Wire Line
	5300 3900 6700 3900
Wire Wire Line
	5500 1400 5500 3800
Wire Wire Line
	5500 3800 6200 3800
Wire Wire Line
	8700 3800 8800 3800
Wire Wire Line
	9300 4000 8700 4000
Wire Wire Line
	8700 4000 8700 3800
Connection ~ 8700 3800
Text Label 11250 7200 0    70   ~ 0
GND
Wire Wire Line
	2500 5000 2500 4900
Wire Wire Line
	2500 4900 1900 4900
Wire Wire Line
	1900 4900 1900 5400
Wire Wire Line
	1900 5400 1900 5900
Wire Wire Line
	1200 5300 1200 5400
Wire Wire Line
	1200 5400 1600 5400
Wire Wire Line
	1600 5400 1900 5400
Wire Wire Line
	1400 5900 1400 5800
Wire Wire Line
	1400 5900 1900 5900
Wire Wire Line
	1600 5400 1600 5500
Connection ~ 1900 5400
Connection ~ 1400 5900
Connection ~ 1600 5400
Text Label 2150 4900 0    20   ~ 0
GND
Wire Wire Line
	5700 6400 5700 6500
Text Label 6100 6400 0    10   ~ 0
GND
Wire Wire Line
	800  4800 2300 4800
Wire Wire Line
	2300 4800 2300 5000
Wire Wire Line
	800  4800 800  6000
Wire Wire Line
	800  6000 6100 6000
Text Label 2025 4800 0    20   ~ 0
VCC
Text Label 11350 5900 0    70   ~ 0
VCC
Wire Wire Line
	8000 4900 8000 3800
Wire Wire Line
	8000 4900 5400 4900
Wire Wire Line
	5400 4900 5400 5000
Wire Wire Line
	7200 3900 7200 3800
Wire Wire Line
	7200 3800 6600 3800
Wire Wire Line
	7200 3900 7100 3900
Connection ~ 7200 3800
Wire Wire Line
	7900 4800 5300 4800
Wire Wire Line
	5300 4800 5300 5000
Wire Wire Line
	6100 6200 700  6200
Wire Wire Line
	700  6200 700  4300
Wire Wire Line
	700  4300 3100 4300
Wire Wire Line
	3100 4300 3100 5000
Text Label 2725 4300 0    20   ~ 0
PIR
Wire Wire Line
	2900 5000 2900 4500
Wire Wire Line
	2900 4500 1200 4500
Wire Wire Line
	1200 4500 1200 4900
Text Label 2675 4500 0    20   ~ 0
DLS
Wire Wire Line
	1000 5900 1000 5800
Wire Wire Line
	1000 5900 900  5900
Wire Wire Line
	900  5900 900  4400
Wire Wire Line
	900  4400 3000 4400
Wire Wire Line
	3000 4400 3000 5000
Connection ~ 1000 5900
Text Label 2725 4400 0    20   ~ 0
BTN1
$Comp
L Nixie_12-9-eagle-import:R-US_0207_10 R3
U 1 0 FFEB715B0833C387
P 9000 3800
F 0 "R3" H 8850 3859 59  0000 L BNN
F 1 "330K" H 8850 3670 59  0000 L BNN
F 2 "Nixie 12-9:0207_10" H 9000 3800 50  0001 C CNN
F 3 "" H 9000 3800 50  0001 C CNN
	1    9000 3800
	1    0    0    -1  
$EndComp
$Comp
L Nixie_12-9-eagle-import:R-US_0207_10 R4
U 1 0 32A97A8B6EDC05E
P 9500 4000
F 0 "R4" H 9350 4059 59  0000 L BNN
F 1 "330K" H 9350 3870 59  0000 L BNN
F 2 "Nixie 12-9:0207_10" H 9500 4000 50  0001 C CNN
F 3 "" H 9500 4000 50  0001 C CNN
	1    9500 4000
	1    0    0    -1  
$EndComp
$Comp
L Nixie_12-9-eagle-import:R-US_0207_10 R1
U 1 0 35828FA7978B36D9
P 6400 3800
F 0 "R1" H 6250 3859 59  0000 L BNN
F 1 "330K" H 6450 3870 59  0000 L BNN
F 2 "Nixie 12-9:0207_10" H 6400 3800 50  0001 C CNN
F 3 "" H 6400 3800 50  0001 C CNN
	1    6400 3800
	1    0    0    -1  
$EndComp
$Comp
L Nixie_12-9-eagle-import:R-US_0207_10 R2
U 1 0 4F92CBDAF8D044C
P 6900 3900
F 0 "R2" H 6750 3759 59  0000 L BNN
F 1 "330K" H 6950 3770 59  0000 L BNN
F 2 "Nixie 12-9:0207_10" H 6900 3900 50  0001 C CNN
F 3 "" H 6900 3900 50  0001 C CNN
	1    6900 3900
	1    0    0    -1  
$EndComp
$Comp
L Nixie_12-9-eagle-import:10-XX S1
U 1 0 5FF56E21CEBFFD5E
P 1200 5800
F 0 "S1" V 950 5700 59  0000 L BNN
F 1 "10-XX" V 1050 5925 59  0000 L BNN
F 2 "Nixie 12-9:B3F-10XX" H 1200 5800 50  0001 C CNN
F 3 "" H 1200 5800 50  0001 C CNN
	1    1200 5800
	0    1    1    0   
$EndComp
$Comp
L Nixie_12-9-eagle-import:RCWL-0516 PIR/RADAR1
U 1 0 9ED99273D1209BB1
P 6400 5700
F 0 "PIR/RADAR1" V 6300 5500 59  0000 R TNN
F 1 "RCWL-0516" H 6650 4600 59  0000 L BNN
F 2 "Nixie 12-9:RCWL-0516" H 6400 5700 50  0001 C CNN
F 3 "" H 6400 5700 50  0001 C CNN
	1    6400 5700
	0    -1   1    0   
$EndComp
$Comp
L Nixie_12-9-eagle-import:GL55 LDR1
U 1 0 96C409F518691D1A
P 1200 5100
F 0 "LDR1" H 1300 5100 59  0000 L BNN
F 1 "5K" H 1300 5000 59  0000 L BNN
F 2 "Nixie 12-9:GL55" H 1200 5100 50  0001 C CNN
F 3 "" H 1200 5100 50  0001 C CNN
	1    1200 5100
	1    0    0    -1  
$EndComp
$Comp
L Nixie_12-9-eagle-import:C-US025-025X050 C3
U 1 0 4E6A3B5410D33965
P 12700 5000
F 0 "C3" H 12740 5025 59  0000 L BNN
F 1 "C-US025-025X050" H 12740 4835 59  0000 L BNN
F 2 "NixieClockMaster:C025-024X044" H 12700 5000 50  0001 C CNN
F 3 "" H 12700 5000 50  0001 C CNN
	1    12700 5000
	1    0    0    -1  
$EndComp
$Comp
L Nixie_12-9-eagle-import:C-US025-025X050 C4
U 1 0 AE746757327B8ADF
P 13550 5100
F 0 "C4" H 13590 5125 59  0000 L BNN
F 1 "C-US025-025X050" H 13590 4935 59  0000 L BNN
F 2 "NixieClockMaster:C025-024X044" H 13550 5100 50  0001 C CNN
F 3 "" H 13550 5100 50  0001 C CNN
	1    13550 5100
	-1   0    0    1   
$EndComp
$Comp
L Nixie_12-9-eagle-import:C-US025-025X050 C5
U 1 0 6D9EE3F9E325A07A
P 13300 5100
F 0 "C5" H 13340 5125 59  0000 L BNN
F 1 "C-US025-025X050" H 13340 4935 59  0000 L BNN
F 2 "NixieClockMaster:C025-024X044" H 13300 5100 50  0001 C CNN
F 3 "" H 13300 5100 50  0001 C CNN
	1    13300 5100
	-1   0    0    1   
$EndComp
$Comp
L Nixie_12-9-eagle-import:C-US025-025X050 C6
U 1 0 17910FFCE451E4D4
P 13000 5100
F 0 "C6" H 13040 5125 59  0000 L BNN
F 1 "C-US025-025X050" H 13040 4935 59  0000 L BNN
F 2 "NixieClockMaster:C025-024X044" H 13000 5100 50  0001 C CNN
F 3 "" H 13000 5100 50  0001 C CNN
	1    13000 5100
	-1   0    0    1   
$EndComp
$Comp
L Nixie_12-9-eagle-import:C-US025-025X050 C2
U 1 0 5683171D733CA19D
P 12400 5000
F 0 "C2" H 12440 5025 59  0000 L BNN
F 1 "C-US025-025X050" H 12440 4835 59  0000 L BNN
F 2 "NixieClockMaster:C025-024X044" H 12400 5000 50  0001 C CNN
F 3 "" H 12400 5000 50  0001 C CNN
	1    12400 5000
	1    0    0    -1  
$EndComp
$Comp
L Nixie_12-9-eagle-import:C-US025-025X050 C1
U 1 0 D1B4F47F3BB99FBE
P 12050 5000
F 0 "C1" H 12090 5025 59  0000 L BNN
F 1 "C-US025-025X050" H 12090 4835 59  0000 L BNN
F 2 "NixieClockMaster:C025-024X044" H 12050 5000 50  0001 C CNN
F 3 "" H 12050 5000 50  0001 C CNN
	1    12050 5000
	1    0    0    -1  
$EndComp
$Comp
L Nixie_12-9-eagle-import:A-NIXIE U$2
U 1 0 AA898CC2A8B04D1A
P 3300 5800
F 0 "U$2" H 3300 5800 50  0001 C CNN
F 1 "A-NIXIE" H 3300 5800 50  0001 C CNN
F 2 "Nixie 12-9:A-NIXIECLASSIC" H 3300 5800 50  0001 C CNN
F 3 "" H 3300 5800 50  0001 C CNN
	1    3300 5800
	1    0    0    -1  
$EndComp
$Comp
L Nixie_12-9-eagle-import:GND #SUPPLY01
U 1 0 57B2A199E46721A6
P 5700 6600
F 0 "#SUPPLY01" H 5700 6600 50  0001 C CNN
F 1 "GND" H 5625 6475 59  0000 L BNN
F 2 "" H 5700 6600 50  0001 C CNN
F 3 "" H 5700 6600 50  0001 C CNN
	1    5700 6600
	1    0    0    -1  
$EndComp
$Comp
L Nixie_12-9-eagle-import:GND #SUPPLY02
U 1 0 484E0C833EFDB7C
P 1600 5600
F 0 "#SUPPLY02" H 1600 5600 50  0001 C CNN
F 1 "GND" H 1525 5475 59  0000 L BNN
F 2 "" H 1600 5600 50  0001 C CNN
F 3 "" H 1600 5600 50  0001 C CNN
	1    1600 5600
	1    0    0    -1  
$EndComp
$Comp
L russian-nixies:IN-12A N3
U 1 1 5CFCD140
P 6500 2000
F 0 "N3" V 6496 1297 50  0000 R CNN
F 1 "IN-12A" V 6405 1297 50  0000 R CNN
F 2 "NixieClockMaster:IN-12" H 6500 2150 50  0001 C CNN
F 3 "" H 6500 2000 50  0001 C CNN
	1    6500 2000
	0    -1   -1   0   
$EndComp
$Comp
L russian-nixies:IN-12A N1
U 1 1 5CFCD91C
P 1900 2000
F 0 "N1" V 1896 1297 50  0000 R CNN
F 1 "IN-12A" V 1805 1297 50  0000 R CNN
F 2 "NixieClockMaster:IN-12" H 1900 2150 50  0001 C CNN
F 3 "" H 1900 2000 50  0001 C CNN
	1    1900 2000
	0    -1   -1   0   
$EndComp
$Comp
L russian-nixies:IN-12A N2
U 1 1 5CFCDAC2
P 3900 2000
F 0 "N2" V 3896 1297 50  0000 R CNN
F 1 "IN-12A" V 3805 1297 50  0000 R CNN
F 2 "NixieClockMaster:IN-12" H 3900 2150 50  0001 C CNN
F 3 "" H 3900 2000 50  0001 C CNN
	1    3900 2000
	0    -1   -1   0   
$EndComp
$Comp
L russian-nixies:IN-12A N4
U 1 1 5CFCDD00
P 8100 2000
F 0 "N4" V 8096 1297 50  0000 R CNN
F 1 "IN-12A" V 8005 1297 50  0000 R CNN
F 2 "NixieClockMaster:IN-12" H 8100 2150 50  0001 C CNN
F 3 "" H 8100 2000 50  0001 C CNN
	1    8100 2000
	0    -1   -1   0   
$EndComp
$Comp
L russian-nixies:IN-12A N5
U 1 1 5CFCDECE
P 11100 2000
F 0 "N5" V 11096 1297 50  0000 R CNN
F 1 "IN-12A" V 11005 1297 50  0000 R CNN
F 2 "NixieClockMaster:IN-12" H 11100 2150 50  0001 C CNN
F 3 "" H 11100 2000 50  0001 C CNN
	1    11100 2000
	0    -1   -1   0   
$EndComp
$Comp
L russian-nixies:IN-12A N6
U 1 1 5CFCDFFE
P 12700 2000
F 0 "N6" V 12696 1297 50  0000 R CNN
F 1 "IN-12A" V 12605 1297 50  0000 R CNN
F 2 "NixieClockMaster:IN-12" H 12700 2150 50  0001 C CNN
F 3 "" H 12700 2000 50  0001 C CNN
	1    12700 2000
	0    -1   -1   0   
$EndComp
NoConn ~ 5500 5000
Wire Wire Line
	4700 1100 4700 1400
$Comp
L Device:LED D1
U 1 1 5D046758
P 5000 1400
F 0 "D1" H 4992 1145 50  0000 C CNN
F 1 "LED" H 4992 1236 50  0000 C CNN
F 2 "NixieClockMaster:LED-5MM-Centred" H 5000 1400 50  0001 C CNN
F 3 "~" H 5000 1400 50  0001 C CNN
	1    5000 1400
	-1   0    0    1   
$EndComp
Wire Wire Line
	7200 3800 7900 3800
Wire Wire Line
	7900 3800 7900 4800
Wire Wire Line
	8000 3800 8700 3800
Wire Wire Line
	5150 1400 5500 1400
Wire Wire Line
	4850 1400 4700 1400
Connection ~ 4700 1400
Wire Wire Line
	4700 1400 4700 2400
$Comp
L Device:LED D2
U 1 1 5D093A63
P 5000 2400
F 0 "D2" H 4992 2145 50  0000 C CNN
F 1 "LED" H 4992 2236 50  0000 C CNN
F 2 "NixieClockMaster:LED-5MM-Centred" H 5000 2400 50  0001 C CNN
F 3 "~" H 5000 2400 50  0001 C CNN
	1    5000 2400
	-1   0    0    1   
$EndComp
Wire Wire Line
	5150 2400 5300 2400
Wire Wire Line
	4850 2400 4700 2400
Wire Wire Line
	8900 1100 8900 1400
$Comp
L Device:LED D3
U 1 1 5D0E13D2
P 9200 1400
F 0 "D3" H 9192 1145 50  0000 C CNN
F 1 "LED" H 9192 1236 50  0000 C CNN
F 2 "NixieClockMaster:LED-5MM-Centred" H 9200 1400 50  0001 C CNN
F 3 "~" H 9200 1400 50  0001 C CNN
	1    9200 1400
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D4
U 1 1 5D0E14C7
P 9200 2400
F 0 "D4" H 9192 2145 50  0000 C CNN
F 1 "LED" H 9192 2236 50  0000 C CNN
F 2 "NixieClockMaster:LED-5MM-Centred" H 9200 2400 50  0001 C CNN
F 3 "~" H 9200 2400 50  0001 C CNN
	1    9200 2400
	-1   0    0    1   
$EndComp
Wire Wire Line
	9350 2400 9600 2400
Wire Wire Line
	9050 2400 8900 2400
Wire Wire Line
	9050 1400 8900 1400
Connection ~ 8900 1400
Wire Wire Line
	8900 1400 8900 2400
Wire Wire Line
	9350 1400 10000 1400
NoConn ~ 6100 5800
NoConn ~ 6100 6600
$Comp
L Jumper:Jumper_3_Open JP1
U 1 1 5D157B28
P 700 3900
F 0 "JP1" H 700 4032 50  0000 C CNN
F 1 "Jumper_3_Open" H 700 4123 50  0000 C CNN
F 2 "NixieClockMaster:SOLDER-JUMPER_2-WAY" H 700 3900 50  0001 C CNN
F 3 "~" H 700 3900 50  0001 C CNN
	1    700  3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	950  3900 1000 3900
Wire Wire Line
	1000 3900 1000 4600
Wire Wire Line
	700  3750 700  1100
Wire Wire Line
	700  1100 4700 1100
Wire Wire Line
	450  3900 300  3900
Wire Wire Line
	300  3900 300  4700
Wire Wire Line
	300  4700 2400 4700
Wire Wire Line
	2400 4700 2400 5000
$Comp
L NixieClockMaster:PWR_FLAG #FLG01
U 1 1 5D195F9B
P 8900 5650
F 0 "#FLG01" H 8900 5915 50  0000 C CNN
F 1 "PWR_FLAG" H 8900 5824 50  0000 C CNN
F 2 "" H 8900 5650 50  0001 C CNN
F 3 "" H 8900 5650 50  0001 C CNN
	1    8900 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 5650 8900 5900
Connection ~ 8900 5900
$Comp
L NixieClockMaster:PWR_FLAG #FLG02
U 1 1 5D1AB730
P 12250 6950
F 0 "#FLG02" H 12450 6850 50  0000 C CNN
F 1 "PWR_FLAG" H 12450 6950 50  0000 C CNN
F 2 "" H 12250 6950 50  0001 C CNN
F 3 "" H 12250 6950 50  0001 C CNN
	1    12250 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	12250 6950 12250 7200
Connection ~ 9600 5900
Wire Wire Line
	5200 4700 5200 5000
$Comp
L LED:WS2812B D6
U 1 1 5D8B9F4D
P 8900 6700
F 0 "D6" H 8950 6950 50  0000 L CNN
F 1 "WS2812B" H 9241 6655 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 8950 6400 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 9000 6325 50  0001 L TNN
	1    8900 6700
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D5
U 1 1 5D8BA2B0
P 8200 6700
F 0 "D5" H 8250 6950 50  0000 L CNN
F 1 "WS2812B" H 8541 6655 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 8250 6400 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 8300 6325 50  0001 L TNN
	1    8200 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 6700 7700 4700
Wire Wire Line
	5200 4700 7700 4700
Wire Wire Line
	5700 6400 6100 6400
$Comp
L LED:WS2812B D7
U 1 1 5D8D1242
P 9600 6700
F 0 "D7" H 9650 6950 50  0000 L CNN
F 1 "WS2812B" H 9941 6655 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 9650 6400 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 9700 6325 50  0001 L TNN
	1    9600 6700
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D8
U 1 1 5D8D13A9
P 10300 6700
F 0 "D8" H 10350 6950 50  0000 L CNN
F 1 "WS2812B" H 10641 6655 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 10350 6400 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 10400 6325 50  0001 L TNN
	1    10300 6700
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D9
U 1 1 5D8D144E
P 11000 6700
F 0 "D9" H 11050 6950 50  0000 L CNN
F 1 "WS2812B" H 11341 6655 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 11050 6400 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 11100 6325 50  0001 L TNN
	1    11000 6700
	1    0    0    -1  
$EndComp
$Comp
L LED:WS2812B D10
U 1 1 5D8D14F8
P 11700 6700
F 0 "D10" H 11750 6950 50  0000 L CNN
F 1 "WS2812B" H 11700 6450 50  0000 L CNN
F 2 "LED_SMD:LED_WS2812B_PLCC4_5.0x5.0mm_P3.2mm" H 11750 6400 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf" H 11800 6325 50  0001 L TNN
	1    11700 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 5900 9600 5900
Wire Wire Line
	11700 7000 11700 7200
Connection ~ 11700 7200
Wire Wire Line
	11000 7000 11000 7200
Connection ~ 11000 7200
Wire Wire Line
	11000 7200 11700 7200
Wire Wire Line
	8200 7200 8200 7000
Wire Wire Line
	8200 7200 8900 7200
Wire Wire Line
	8900 7000 8900 7200
Connection ~ 8900 7200
Wire Wire Line
	8900 7200 9600 7200
Wire Wire Line
	9600 7000 9600 7200
Connection ~ 9600 7200
Wire Wire Line
	9600 7200 10300 7200
Wire Wire Line
	10300 7000 10300 7200
Connection ~ 10300 7200
Wire Wire Line
	10300 7200 11000 7200
Wire Wire Line
	8200 6400 8200 5900
Wire Wire Line
	8900 5900 8900 6400
Wire Wire Line
	9600 5900 9600 6400
Wire Wire Line
	10300 5900 10300 6400
Connection ~ 10300 5900
Wire Wire Line
	11000 6400 11000 5900
Connection ~ 11000 5900
Wire Wire Line
	10300 5900 11000 5900
Wire Wire Line
	9600 5900 10300 5900
Wire Wire Line
	11000 5900 11700 5900
Wire Wire Line
	11700 6400 11700 5900
Wire Wire Line
	11400 6700 11300 6700
Wire Wire Line
	7900 6700 7700 6700
Wire Wire Line
	8500 6700 8600 6700
Wire Wire Line
	9200 6700 9300 6700
Wire Wire Line
	9900 6700 10000 6700
Wire Wire Line
	12050 4900 12050 4750
Wire Wire Line
	12050 4750 12400 4750
Wire Wire Line
	13550 4750 13550 4900
Wire Wire Line
	13550 5200 13550 5400
Wire Wire Line
	13550 5400 13300 5400
Wire Wire Line
	12050 5400 12050 5200
Wire Wire Line
	12400 5400 12400 5200
Connection ~ 12400 5400
Wire Wire Line
	12400 5400 12050 5400
Wire Wire Line
	12700 5400 12700 5200
Connection ~ 12700 5400
Wire Wire Line
	12700 5400 12400 5400
Wire Wire Line
	13000 5400 13000 5200
Connection ~ 13000 5400
Wire Wire Line
	13000 5400 12850 5400
Wire Wire Line
	13300 5400 13300 5200
Connection ~ 13300 5400
Wire Wire Line
	13300 5400 13000 5400
Wire Wire Line
	13300 4900 13300 4750
Connection ~ 13300 4750
Wire Wire Line
	13300 4750 13550 4750
Wire Wire Line
	13000 4900 13000 4750
Connection ~ 13000 4750
Wire Wire Line
	13000 4750 13300 4750
Wire Wire Line
	12700 4900 12700 4750
Connection ~ 12700 4750
Wire Wire Line
	12700 4750 13000 4750
Wire Wire Line
	12400 4900 12400 4750
Connection ~ 12400 4750
Wire Wire Line
	12400 4750 12700 4750
Text Label 12500 4750 0    70   ~ 0
VCC
Wire Wire Line
	12850 5400 12850 5500
$Comp
L Nixie_12-9-eagle-import:GND #SUPPLY0101
U 1 0 5DC5CF48
P 12850 5600
F 0 "#SUPPLY0101" H 12850 5600 50  0001 C CNN
F 1 "GND" H 12775 5475 59  0000 L BNN
F 2 "" H 12850 5600 50  0001 C CNN
F 3 "" H 12850 5600 50  0001 C CNN
	1    12850 5600
	1    0    0    -1  
$EndComp
Connection ~ 12850 5400
Wire Wire Line
	12850 5400 12700 5400
Wire Wire Line
	10600 6700 10700 6700
Wire Wire Line
	8200 5900 8900 5900
Wire Wire Line
	11700 7200 12250 7200
NoConn ~ 12000 6700
Text Label 1000 4200 1    20   ~ 0
SEP_SUPPLY_HV
Text Label 300  4350 1    20   ~ 0
SEP_SUPPLY_VIN
Text Label 700  3500 1    20   ~ 0
SEP_SUPPLY
Text Label 8800 3500 0    20   ~ 0
C_0
Text Label 8800 3400 0    20   ~ 0
C_9
Text Label 8800 3300 0    20   ~ 0
C_8
Text Label 8800 3200 0    20   ~ 0
C_7
Text Label 8800 3100 0    20   ~ 0
C_6
Text Label 8800 3000 0    20   ~ 0
C_5
Text Label 8800 2900 0    20   ~ 0
C_4
Text Label 8800 2800 0    20   ~ 0
C_3
Text Label 8800 2700 0    20   ~ 0
C_2
Text Label 8800 2600 0    20   ~ 0
C_1
$EndSCHEMATC
